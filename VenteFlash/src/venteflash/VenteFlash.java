package venteflash;

public class VenteFlash {
    
    public static Database database;

    public static void main(String[] args) throws Exception {

        // Vérifier que l'utilisateur a fourni un argument.
        if(args.length == 0)
        {
            throw new Exception("Aucun argument n'a été fourni !");
        }

        database = new Database();
        database.connect(args[0]);

    }

}
