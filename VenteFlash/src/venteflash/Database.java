package venteflash;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/*
 * Servez-vous de cette classe pour établir la connexion et pour
 * effectuer vos requêtes à la base postgresql.
 */

public class Database {
    
    /*
     * Ces trois variables permettront à votre application de se connecter à votre base postgresql
     * du DIRO.
     * 
     * -> Mettez votre nom d'usager du DIRO à la fin de l'url.
     * -> Mettez votre nom d'usager avec _app en suffixe pour la variable user.
     * -> Votre mot de passe est forcément le suivant :
     *     <> Les trois dernières lettres, en minuscule, de votre nom d'usager du DIRO.
     *     <> Suivi des quatre premiers caractères de votre login Studium en minuscule.
     *     <> Suivi de la première lettre de votre nom de famille en majuscule.
     *     
     * Par exemple, pour un étudiant nommé Simon Tremblay dont le nom d'usager du DIRO est trembsim
     * et dont le login Studium est p1110293, le mot de passe sera simp111T. D'ailleurs, il devra
     * mettre trembsim_app pour la variable user et trembsin à la fin de l'url.
     */
    
    private final String url = "jdbc:postgresql://postgres.iro.umontreal.ca:5432/jalbertk";
    private final String user = "jalbertk_app";
    private final String password = "rtkp117J";
 
    /*
     * La fonction ci-dessous est un exemple de connexion à la base avec un exemple de requête.
     * Évidemment, la fonction ne sera pas utilisable dans l'état car la requête ne correspond
     * pas à votre base de données. Ce n'est qu'un exemple.
     * 
     * Assurez-vous d'avoir installé JDBC et d'avoir importé son .jar dans votre projet !
     * Télécharger JDBC : https://jdbc.postgresql.org/download.html
     * 
     * De plus, assurez-vous d'être connecté au VPN de l'université avant de lancer votre programme !
     * Instructions VPN : https://bib.umontreal.ca/public/bib/soutien-informatique/S13-VPN.pdf
     * 
     * Assurez-vous d'avoir donné les droits à votre base postgreSQL pour faire des requêtes !
     * Instructions pour donner les droits : https://tableplus.com/blog/2018/04/postgresql-how-to-grant-access-to-users.html
     */

    public void connect(String req) {
        Connection c = null;
        try {
           // Établissement de la connexion avec les variables fournies plus haut
           c = DriverManager.getConnection(url, user, password);

            switch(req) {
                case "1" : requete_un(c); break; // Résultat de la requête #1...
                case "2" : requete_deux(c); break; // Résultat de la requête #2...
                case "3" : requete_trois(c); break; // ...
                case "4" : requete_quatre(c); break;
                case "5" : requete_cinq(c); break;
                case "6" : requete_six(c); break;
                case "7" : requete_sept(c); break;
                case "8" : requete_huit(c); break;
                default: {
                    // Vérifie que l'utilisateur fourni un argument représentant une requête existante
                    throw new Exception("Numéro de la requête doit être entre 1 et 8 !");
                }
            }

        } catch (Exception e) {
           e.printStackTrace();
           System.err.println(e.getClass().getName()+": "+e.getMessage());
        }
        
    }

    // Requête #1 : Nombre d'éléments vendus par catégorie pour le vendeur à l'IDA 1.
    public void requete_un(Connection c) {

        String requete1 = "SELECT categ, count(ida) AS nbelem\n" +
                "FROM venteflash.produit\n" +
                "WHERE ida=1 AND status=true " +
                "GROUP BY categ;";

        try {
            Statement requete = c.createStatement();

            ResultSet rs = requete.executeQuery(requete1);

            System.out.println("categ | nbelem");

            while(rs.next())
            {
                String categ = rs.getString("categ");
                int nbelem = rs.getInt("nbelem");
                System.out.println(categ + " | " + nbelem);

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
        }
    }

    // Requête #2 : Nombre d'élément à vendre et vendu par catégorie et leur total.
    public void requete_deux(Connection c) {
        String requete1 = "WITH vendre AS (\n" +
                "\tSELECT categ, count(idp) as nbelem_vendre\n" +
                "\tFROM venteflash.produit\n" +
                "\tWHERE status=false\n" +
                "\tGROUP BY categ\n" +
                "),\n" +
                "\n" +
                "vendu AS (\n" +
                "\tSELECT categ, count(idp) as nbelem_vendu\n" +
                "\tFROM venteflash.produit\n" +
                "\tWHERE status=true\n" +
                "\tGROUP BY categ\n" +
                "),\n" +
                "\n" +
                "total_vendre AS (\n" +
                "\tSELECT sum(nbelem_vendre) as nbelem_vendre \n" +
                "\tFROM vendre\t\n" +
                "),\n" +
                "\n" +
                "total_vendu AS (\n" +
                "\tSELECT sum(nbelem_vendu) as nbelem_vendu \n" +
                "\tFROM vendu\t\n" +
                "),\n" +
                "\n" +
                "result AS (\n" +
                "\tSELECT DISTINCT produit.categ, nbelem_vendre, nbelem_vendu\n" +
                "\tFROM venteflash.produit\n" +
                "\tFULL JOIN vendre ON produit.categ = vendre.categ\n" +
                "\tFULL JOIN vendu ON produit.categ = vendu.categ\n" +
                "\tORDER BY produit.categ\n" +
                ")\n" +
                "\n" +
                "select * from result\n" +
                "UNION ALL\n" +
                "SELECT 'total' as categ, nbelem_vendre, nbelem_vendu\n" +
                "FROM total_vendre, total_vendu;";

        try {
            Statement requete = c.createStatement();

            ResultSet rs = requete.executeQuery(requete1);

            System.out.println("categ | nbelem_vendre | nbelem_vendu");

            while(rs.next())
            {
                String categ = rs.getString("categ");
                int nbelem_vendre = rs.getInt("nbelem_vendre");
                int nbelem_vendu = rs.getInt("nbelem_vendu");
                System.out.println(categ + " | " + nbelem_vendre + " | " + nbelem_vendu);

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
        }
    }

    // Requête #3 : Indicatif régional le plus commun pour les annonceurs.
    public void requete_trois(Connection c) {
        String requete1 = "WITH region AS (\n" +
                "\tSELECT SUBSTRING(tel, 1, 3) as indicatif, count(ida) as nbannonceur \n" +
                "\tFROM venteflash.annonceur\n" +
                "\tGROUP BY indicatif\n" +
                "),\n" +
                "\n" +
                "max AS (\n" +
                "\tSELECT max(nbannonceur)\n" +
                "\tFROM region\n" +
                ")\n" +
                "\n" +
                "SELECT indicatif\n" +
                "FROM region, max\n" +
                "WHERE nbannonceur=max;";

        try {
            Statement requete = c.createStatement();

            ResultSet rs = requete.executeQuery(requete1);

            System.out.println("indicatif");

            while(rs.next())
            {
                String indicatif = rs.getString("indicatif");
                System.out.println(indicatif);

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
        }
    }

    // Requête #4 : Nombre d'éléments vendus après un changement de prix.
    public void requete_quatre(Connection c) {
        String requete1 = "SELECT count(p.idp) as nbelem_vendu\n" +
                "FROM venteflash.price_change AS c, venteflash.produit AS p\n" +
                "WHERE c.idp=p.idp and status=true and accept=true; ";

        try {
            Statement requete = c.createStatement();

            ResultSet rs = requete.executeQuery(requete1);

            System.out.println("nbelem_vendu");

            while(rs.next())
            {
                int nbelem_vendu = rs.getInt("nbelem_vendu");
                System.out.println(nbelem_vendu);

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
        }
    }

    // Requête #5 : Nombre d'éléments à vendre et le prix moyen pour chaque annonceur.
    public void requete_cinq(Connection c) {
        String requete1 = "WITH temp AS (\n" +
                "\tSELECT ida, count(idp) as qte_produit, sum(prixp)/count(idp) as prix_moyen \n" +
                "\tFROM venteflash.produit\n" +
                "\tWHERE status=false\n" +
                "\tGROUP BY ida\n" +
                ")\n" +
                "\n" +
                "SELECT prea as prenom, noma as nom, qte_produit, ROUND(CAST (prix_moyen AS int),2) as prix_moyen\n" +
                "FROM temp, venteflash.annonceur\n" +
                "WHERE temp.ida=annonceur.ida;";

        try {
            Statement requete = c.createStatement();

            ResultSet rs = requete.executeQuery(requete1);

            System.out.println("prenom | nom | qte_produit | prix_moyen");

            while(rs.next())
            {
                String prenom = rs.getString("prenom");
                String nom = rs.getString("nom");
                int qte_produit = rs.getInt("qte_produit");
                int prix_moyen = rs.getInt("prix_moyen");
                System.out.println(prenom + " | " + nom + " | " + qte_produit + " | " + prix_moyen );

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
        }
    }

    // Requête #6 : Nom et prenom des experts ayant fait le plus et le moins de modifications de prix qui ont
    //              été acceptées.
    public void requete_six(Connection c) {
        String requete1 = "WITH temp AS (\n" +
                "\tSELECT nox, count(idp) AS nbelem\n" +
                "\tFROM venteflash.price_change\n" +
                "\tWHERE accept=true\n" +
                "\tGROUP BY nox\n" +
                "),\n" +
                "\n" +
                "extremum AS (\n" +
                "\tSELECT max(nbelem) as max, min(nbelem) as min\n" +
                "\tFROM temp\n" +
                ")\n" +
                "\n" +
                "(SELECT 'max' as extremum, prex as prenom, nomx as nom\n" +
                "FROM venteflash.expert, temp, extremum\n" +
                "WHERE nbelem = max and expert.nox = temp.nox)\n" +
                "UNION ALL\n" +
                "(SELECT 'min' as extremum, prex as prenom, nomx as nom\n" +
                "FROM venteflash.expert, temp, extremum\n" +
                "WHERE nbelem = min and expert.nox = temp.nox);";

        try {
            Statement requete = c.createStatement();

            ResultSet rs = requete.executeQuery(requete1);

            System.out.println("extremum | prenom | nom ");

            while(rs.next())
            {
                String extremum = rs.getString("extremum");
                String prenom = rs.getString("prenom");
                String nom = rs.getString("nom");
                System.out.println(extremum + " | " + prenom + " | " + nom);

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
        }
    }

    // Requête #7 : Nombre de modifications acceptées selon l'année.
    public void requete_sept(Connection c) {
        String requete1 = "WITH temp AS (\n" +
                "\tSELECT idp, accept, EXTRACT(YEAR FROM datec) as datec\n" +
                "\tFROM venteflash.price_change\n" +
                ")\n" +
                "\n" +
                "SELECT datec, count(idp) as elem_modifie\n" +
                "FROM temp\n" +
                "WHERE accept=true\n" +
                "GROUP BY datec;";

        try {
            Statement requete = c.createStatement();

            ResultSet rs = requete.executeQuery(requete1);

            System.out.println("datec | elem_modifie");

            while(rs.next())
            {
                String datec = rs.getString("datec");
                int elem_modifie = rs.getInt("elem_modifie");
                System.out.println(datec + " | " + elem_modifie);

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
        }
    }

    // Requête #8 : Moyenne de prix par categorie.
    public void requete_huit(Connection c) {
        String requete1 = "WITH temp AS (\n" +
                "\tSELECT categ, avg(prixp) AS prix_moyen, count(idp) as nbproduit \n" +
                "\tFROM venteflash.produit \n" +
                "\tGROUP BY categ\n" +
                ")\n" +
                "\n" +
                "SELECT categ, ROUND(CAST(prix_moyen AS int), 2) as prix_moyen, nbproduit\n" +
                "FROM temp\n" +
                "ORDER BY prix_moyen;";

        try {
            Statement requete = c.createStatement();

            ResultSet rs = requete.executeQuery(requete1);

            System.out.println("categ | prix_moyen | nbproduit");

            while(rs.next())
            {
                String categ = rs.getString("categ");
                int prix_moyen = rs.getInt("prix_moyen");
                int nbproduit = rs.getInt("nbproduit");
                System.out.println(categ + " | " + prix_moyen + " | " + nbproduit);

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
        }
    }

}
